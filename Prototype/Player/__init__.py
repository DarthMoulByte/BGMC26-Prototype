from bgez.framework.types import Mapped, Asset, GameObject
from bgez.framework.types import CameraObject
from bgez.framework.objects import Input

from bgez.framework.inputs import ButtonMapping, KeyboardButton, MouseButton
from bgez.framework.inputs import AxisMapping, MouseAxis

from bgez.framework import events

from mathutils import Vector, Matrix, Quaternion
from math import sin, cos, radians, degrees, copysign
from bgez import constraints
from bgez import render
from bgez import utils
import random
import time
import aud

class Player(Asset, GameObject):
    Reference = './Prototype/Player/Player:Player'
    Register = True

    VIEW_VERTICAL_MAX = radians(60)
    VIEW_VERTICAL_MIN = radians(-60)

    HEAD_LERP = .3
    ALPHA_LERP = .2
    POINTER_LERP = .3

    JUMP_SPEED = 4 # Speed increment
    SPRINT = 1.2 # Multiplier
    TORQUE = 8 # Torque

    VOLUME_MAX = .3
    PITCH_MIN = .3
    PITCH_MAX = 1.5
    ROLL = aud.Factory.sine(300)

    controls = Input('PlayerControls', lockedMouse=True).add(

        # Mouse movement
        AxisMapping('viewx', MouseAxis('x')),
        AxisMapping('viewy', MouseAxis('y')),

        # Button mappings
        ButtonMapping('forward',
            KeyboardButton(Input.Keys.ZKEY), # AZERTY
            KeyboardButton(Input.Keys.WKEY)), # QWERTY
        ButtonMapping('backward',
            KeyboardButton(Input.Keys.SKEY)),
        ButtonMapping('left',
            KeyboardButton(Input.Keys.QKEY), # AZERTY
            KeyboardButton(Input.Keys.AKEY)), # QWERTY
        ButtonMapping('right',
            KeyboardButton(Input.Keys.DKEY)),

        ButtonMapping('sprint',
            KeyboardButton(Input.Keys.LEFTSHIFTKEY)),
        ButtonMapping('jump',
            KeyboardButton(Input.Keys.SPACEKEY)),
        ButtonMapping('respawn',
            KeyboardButton(Input.Keys.RKEY)),

        ButtonMapping('fire',
            KeyboardButton(Input.Keys.EKEY),
            KeyboardButton(Input.Keys.FKEY),
            MouseButton(Input.Keys.LEFTMOUSE),
            MouseButton(Input.Keys.RIGHTMOUSE)),
    )

    def construct(self):
        self.camera = CameraObject(self.childrenRecursive.get('Camera'))
        self.headPivot = GameObject(self.childrenRecursive.get('HeadPivot'))
        self.head = GameObject(self.childrenRecursive.get('Head'))
        self.wheel = GameObject(self.childrenRecursive.get('Wheel'))
        self.pointer = GameObject(self.childrenRecursive.get('Pointer'))
        self.circle = GameObject(self.childrenRecursive.get('Circle'))

        self.sound = aud.device().play(self.ROLL)
        self.sound.volume = 0

        self.relatives.extend((
            self.head,
            self.wheel,
        ))

        # Goal of the game
        self.data = 0

        # Calculating the distance between player and wheel
        self.wheelDelta = self.wheel.worldPosition - self.worldPosition
        self.wheel.removeParent()

        # Setting up the pivot constraint for the wheel
        self.wheelConstraint = constraints.createConstraint(
            self.getPhysicsId(), self.wheel.getPhysicsId(),
            constraints.GENERIC_6DOF_CONSTRAINT,
            pivot_x = self.wheelDelta.x,
            pivot_y = self.wheelDelta.y,
            pivot_z = self.wheelDelta.z,
            flag = 128,
        )

        # Set the cam as active
        self.camera.setCamera()
        self.head.removeParent()

        # The pointed object
        self.pointedAt = None

        # Ground hit reset
        self.ground = None
        @self.wheel.collisionCallbacks.append
        def ground(object, point, normal):
                self.ground = -normal

    def destroy(self):
        constraints.removeConstraint(self.wheelConstraint.getConstraintId())

    def moveTo(self, position):
        self.wheel.setLinearVelocity((0, 0, 0))
        self.wheel.worldPosition = position + self.wheelDelta
        self.setLinearVelocity((0, 0, 0))
        self.worldPosition = position

    @Mapped.bind('respawn')
    def respawn(self, event):
        events.trigger('respawn', self.scene)

    def addBit(self, *_):
        vertex = self.circle.meshes[0].getVertex(0, self.data)
        bit = self.scene.addObject('Bit')
        bit.worldPosition = (self.circle.worldOrientation * vertex.XYZ
            + self.circle.worldPosition)
        bit.setParent(self.circle)
        self.data += 1

        if self.data >= 8: # trigger the event
            events.trigger('gameover', self.scene)

    def pre_draw(self, scene):
        if scene is self.scene:

            # Head rotation slow parent
            self.head.worldPosition = self.headPivot.worldPosition
            self.head.worldOrientation = self.head.worldOrientation.lerp(
                self.headPivot.worldOrientation, self.HEAD_LERP)

            # Wheel sound control
            velocity = self.wheel.getAngularVelocity().magnitude
            self.sound.volume = utils.clamp(
                self.VOLUME_MAX * velocity / 30,
                min=0, max=self.VOLUME_MAX)
            self.sound.pitch = utils.clamp(
                self.PITCH_MAX * velocity / 200,
                min=self.PITCH_MIN, max=self.PITCH_MAX)
            # self.sound.pitch *= 1 + (2 * random.random() - 1) * .01

            # Pointer slow following using rayCast
            hit, position, normal = self.head.localRayCast((0, 1024, 0),
                mask = utils.invert(self.wheel.collisionGroup))
            delta = position - self.pointer.worldPosition
            self.pointer.alignAxisToVect(delta, 1, self.POINTER_LERP)

            # Fade the head depending on distance to object pointed
            distance = (self.head.worldPosition - position).magnitude
            color = self.head.color.copy()
            color.w = utils.clamp(distance / 2, min = .5, max = 1)
            self.head.color = self.head.color.lerp(color, self.ALPHA_LERP)

            # Display laser
            if hit and hit.get('interactive', False) \
            and delta.magnitude < 1:
                self.pointedAt = hit
                render.drawLine(
                    self.pointer.worldPosition,
                    position, (1, 1, 1))

            else:
                self.pointedAt = None

    @Mapped.bind('viewx', 'viewy')
    def look(self, event):
        self.applyRotation((0, 0, event.viewx), False)
        self.headPivot.applyRotation((event.viewy, 0, 0), True)

        # Vertical view lock
        vertical = self.headPivot.localOrientation.to_euler()
        vertical.x = utils.clamp(vertical.x,
            min=self.VIEW_VERTICAL_MIN, max=self.VIEW_VERTICAL_MAX)
        self.headPivot.localOrientation = vertical

    @Mapped.bind('fire')
    def fire(self, event):
        if self.pointedAt:
            for callback in self.pointedAt.getMappedCallbacks('interact'):
                callback()

    @Mapped.bind('jump')
    def jump(self, event):
        if self.ground:
            velocity = self.ground * self.JUMP_SPEED
            self.setLinearVelocity(velocity + self.getLinearVelocity())
            self.ground = None

    @Mapped.bind('forward', 'backward', 'left', 'right', 'sprint')
    def move(self, event):
        direction = Vector((0, 0, 0))
        camera = utils.getCamera()

        frontAxis = camera.getAxisVect((0, 0, -1))
        frontAxis.z = 0

        angle = copysign(frontAxis.angle((0, 1, 0)), -frontAxis.x)
        orientation = Matrix.Rotation(angle, 4, (0, 0, 1))

        if event.forward:
            direction.x -= 1
        if event.backward:
            direction.x += 1
        if event.left:
            direction.y -= 1
        if event.right:
            direction.y += 1

        direction.normalize()
        rotation = orientation * direction

        if event.sprint:
            rotation *= self.SPRINT

        self.wheel.applyTorque(rotation * self.TORQUE)
