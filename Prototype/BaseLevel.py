from bgez.framework.libraries import AssetLibrary
from bgez.framework.objects import Stage
from bgez.framework import events
from bgez import Service
from bgez import logic
from bgez import utils
from bgez import log

from mathutils import Vector
import aud

from Prototype.Player import *

@Service.StageManager.MainStage
class BaseStage(Stage):
    '''This is the default behaviour for every level'''

    DEVICE = aud.device()
    DEVICE.volume = .4

    MUSICS = [
        './Prototype/Sounds/Dlay_13_where.mp3',
    ]
    MUSIC = aud.Factory(
        utils.getResourcePath('./Prototype/Sounds/Dlay_13_where.mp3'))
    HANDLE = DEVICE.play(MUSIC)

    DEPENDENCIES = [
        Player.Library(),
    ]

    START_LEVEL = 'Test'
    LEVELS = { # Transitions
        'Test':1, #1:2, 2:3, 3:4
    }

    # World parameters
    backgroundColor = Vector((.051, .051, .051))
    worldMistValues = Vector((0, 16, 128))

    def __init__(self, *args, level=START_LEVEL, **kargs):
        super().__init__(*args, **kargs)
        self.DEPENDENCIES = self.__class__.DEPENDENCIES + [
            AssetLibrary('./Prototype/Levels/Level{}'.format(level), async=True)
        ]; self.level = level

    @staticmethod
    async def fade(scene,
        startValues, endValues, endColor, startColor=None,
        frames=30):

        startColor = Vector(startColor or scene.world.backgroundColor)
        endColor = Vector(endColor)

        startValues = Vector(startValues)
        endValues = Vector(endValues)

        scene.world.mistColor = startColor
        scene.world.backgroundColor = startColor
        scene.world.mistIntensity = startValues.x
        scene.world.mistStart = startValues.y
        scene.world.mistDistance = startValues.z

        for i in range(frames):

            await events.skip()
            lerp = (i + 1) / frames
            transition = startValues.lerp(endValues, lerp)

            scene.world.mistColor = startColor.lerp(endColor, lerp)
            scene.world.backgroundColor = startColor.lerp(endColor, lerp)
            scene.world.mistIntensity = transition.x
            scene.world.mistStart = transition.y
            scene.world.mistDistance = transition.z

    async def fadeOut(self, scene, *args):
        await self.fade(scene,
            (1, 0, 0), self.worldMistValues, self.backgroundColor,
            *args)

    async def fadeIn(self, scene, color=(0, 0, 0), *args):
        await self.fade(scene,
            self.worldMistValues, (1, 0, 0), color,
            *args)

    async def respawn(self, scene):
        if self.alive:
            with self.alive:
                await self.fadeIn(scene)
                await events.skip(frames=5)
                self.player.moveTo(self.spawner.worldPosition)
                await self.fadeOut(scene)

    async def gameover(self, scene):
        await self.fadeIn(scene, (1, 1, 1))
        # await events.skip(frames=60)

        if self.level in self.LEVELS:

            next = self.__class__(level = self.LEVELS[self.level])
            Service.StageManager.replaceStage(self, next)
            scene.clearObjects(keep=[self.sun])

        else: logic.endGame()
        await events.skip()

    def start(self, scene):
        self.handle = events.Handle()
        self.alive = utils.Bool(True)

    def ready(self, scene):
        log.d(logic.LibList(), tag='LibLoad')
        Service.EntityManager.autoLink(*scene.objects)
        events.registerListener(self.respawn, 'respawn', handle=self.handle)
        events.registerListener(self.gameover, 'gameover', handle=self.handle)
        events.execute(self.fadeOut(scene))

        self.sun = scene.objects.get('Sun')
        self.spawner = scene.objects.get('PlayerSpawner')
        self.player = scene.addAsset(Player, reference = self.spawner)
        events.registerListener(self.player.addBit, 'bit', handle=self.handle)
        # self.player.data = 7
        Player.controls.bind(self.player)

    def pre_draw(self, scene):
        if (self.loaded
        and not self.player.invalid
        and scene is self.player.scene):

            if self.alive and self.player.worldPosition.z < -32:
                events.trigger('respawn', scene)

            self.sun.worldPosition = self.player.worldPosition \
            + self.sun.worldOrientation * Vector((0, 0, 128))

    def finalize(self):
        self.handle.unregister()
